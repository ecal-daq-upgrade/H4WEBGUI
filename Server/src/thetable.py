from threading import Thread
import math
import sys
import argparse
import time
from bs4 import BeautifulSoup as Soup
from zmq import *
import socket
#import serial
from opcua import Client
from opcua import ua

 
class TableContError(Exception):
  pass

### Table Controller ###
class TableController:
  def __init__(self):
    
    self.log_file = None

    #Table Controller Parameters
    self.delay_time = 0		#milliseconds
    self.table_display = None	#string
    self.drives = []		#2 elements
    self.client = None

    #Table limits
    #self.x_lower = -5.34
    #self.x_upper = 404.50
    #self.z_lower = 0.00
    #self.z_upper = 338.00 #limit with the pipe in the way
    self.x_lower = 0.
    self.x_upper = 100000
    self.z_lower = 0.
    self.z_upper = 63000 #limit with the pipe in the way
        
    #flags
    self.run = False
    self.table_sim = False		
    
    #position data
    self.move_pos = [-math.inf, -math.inf]		#2 elements 
    self.actual_pos = [-math.inf, -math.inf]	#2 elements
    self.motion_sequence=[5,5]
    self.stop= [True,True]     
    self.fast=[False,False]
    self.status =[-1,-1]
    self.same_pos=[False,False]
    self.error_flag=[False,False]
    self.yellow_box= False

    #serial data
    self.port_pub = 0		#long - for the visa i think    
    self.visa_in = None		#string
    self.serial_name = None
	
  def delay(self):
    time.sleep(self.delay_time/1000)	#delay is in ms
    return

  def get_position(self,index):
    try:
        type_motion=self.select_drive(index)
        #print(motion,abbr_motion)
        #return_pos = self.client.get_node(ua.NodeId.from_string('ns=3;s="Horizontal_Motion"."EncCount_Hor"')).get_value()
        return_pos = self.client.get_node(ua.NodeId.from_string('ns=3;s="'+type_motion[0]+'_Motion"."EncCount_'+type_motion[1]+'"')).get_value()
    except TableContError as error:
      raise error
    return return_pos

  def connect(self):
        self.client = Client(url='opc.tcp://128.141.149.241:4840')
        try:
                self.client.connect()
                root = self.client.get_root_node()
                definition = self.client.load_type_definitions()
                print("Root node is: ", root)
                #print("definitions are: ", definition)
                objects = self.client.get_objects_node()
                print("Objects node is: ", objects)
                print("Children of root are: ", root.get_children())
                for index in 0, 1:
                    self.actual_pos[index] = self.get_position(index) 
                print(self.actual_pos)

        except TableContError as error:
            raise error
        self.table_sim=True
        return self.client



  def initialize_tab_cont(self):
    self.delay_time = 100
    self.drives = ["2","1"] #X=2, Z=1
    sys.stdout = open(self.log_file, 'w')    
    if not self.table_sim:
      self.connect() 
      #self.visa_in.close()

  def move_drive(self, drives_index, pos_index, fast):
    try: 
        type_motion = self.select_drive(drives_index)
        if(self.move_pos[drives_index]-self.actual_pos[drives_index]>0):
            if fast:
                self.motion_sequence[drives_index]=3
                self.set_position((self.move_pos[pos_index]-3000),drives_index)
            else:
                self.motion_sequence[drives_index]=1
                self.set_position((self.move_pos[pos_index]),drives_index)
        elif(self.move_pos[drives_index]-self.actual_pos[drives_index]<0):
            if fast: 
                self.motion_sequence[drives_index]=2
                self.set_position((self.move_pos[pos_index]+3000),drives_index)
            else:
                self.motion_sequence[drives_index]=0
                self.set_position((self.move_pos[pos_index]),drives_index)
    except TableContError as error:
      raise error
    print("exiting move_drive")
    #return (motion,abbr_motion)

  def check_stop(self):
    try:
        for index in 0,1:
            self.stop[index]=False
            type_motion=self.select_drive(index)
            self.stop[index]= self.client.get_node(ua.NodeId.from_string('ns=3;s="PLC_DO_'+type_motion[1]+'_Stop"')).get_value()
        return self.stop
    except TableContError as error:
      raise error
        


  def select_drive(self, index):
        if (index == 0):
            motion="Horizontal"
            abbr_motion="Hor"
        else:
            motion="Vertical"
            abbr_motion="Ver"
        return(motion,abbr_motion)


  def init_table(self,index):
    try:
        self.same_pos[index]=False
        self.fast[index]=False
        self.motion_sequence[index]=5
        self.stop[index]=False
        type_motion = self.select_drive(index)
        self.actual_pos[index]=self.get_position(index)
        self.status[index]= self.client.get_node(ua.NodeId.from_string('ns=3;s="'+type_motion[0]+'_Driver_CmdStatus"."StateCommands"')).get_value()
        print("Status ",self.status[index],"Motion ",type_motion[0])
        while (self.status[index]!=16):
            print("Status not 16")
                #self.client.get_node(ua.NodeId.from_string('ns=3;s="'+type_motion[0]+'_Driver_CmdStatus"."StateCommands"')).set_value(ua.DataValue(ua.Variant(0, ua.VariantType.Byte)))
            self.client.get_node(ua.NodeId.from_string('ns=3;s="'+type_motion[0]+'_Driver_CmdStatus"."StateCommands"')).set_value(ua.DataValue(ua.Variant(1, ua.VariantType.Byte)))
            self.client.get_node(ua.NodeId.from_string('ns=3;s="'+type_motion[0]+'_Driver_CmdStatus"."StateCommands"')).set_value(ua.DataValue(ua.Variant(2, ua.VariantType.Byte)))
            self.client.get_node(ua.NodeId.from_string('ns=3;s="'+type_motion[0]+'_Driver_CmdStatus"."StateCommands"')).set_value(ua.DataValue(ua.Variant(16, ua.VariantType.Byte)))
            self.status[index] = self.client.get_node(ua.NodeId.from_string('ns=3;s="'+type_motion[0]+'_Driver_CmdStatus"."StateCommands"')).get_value() 
    except TableContError as error:
      raise error

  def move_decision(self,index,Runned=False):
    try:
        if (self.actual_pos[index]<self.move_pos[index]+50) and (self.actual_pos[index]>self.move_pos[index]-50):
                print("Same position for", index,"Difference is",self.actual_pos[index]-self.move_pos[index], "<100")
                self.same_pos[index]=True
                return
        if ((abs(self.actual_pos[index]-self.move_pos[index])>3000) and (not Runned)):
                self.fast[index]=True
        else:
                self.fast[index]=False
        if not self.same_pos[index]:
            self.move_drive(index, index, self.fast[index])
            self.start_mov(index)
    except TableContError as error:
      raise error


  def move_to_pos(self, First=True):
    print("Moving table to: ",self.move_pos)
    try:
      if (self.move_pos[0] > self.x_upper) or (self.move_pos[0] < self.x_lower) or (self.move_pos[1] > self.z_upper) or (self.move_pos[1] < self.z_lower):
        #raise TableContError("Specified location is out of bounds.")
        return True
      if First:
        for index in 0,1:
            self.init_table(index)

        for index in 0, 1:
            self.move_decision(index)

       # for index in 0,1:
       #   self.actual_pos[index]=self.get_position(index)

      else:
        for index in 0,1:
            if self.fast[index]:
                self.move_decision(index,self.fast[index])
    

        #for index in 0,1:
        #  self.actual_pos[index]=self.get_position(index)
      return False
    except TableContError as error:
      raise error

  def set_position(self, pos, index):
    try:
        type_motion=self.select_drive(index)
        print(type_motion)
        self.client.get_node(ua.NodeId.from_string('ns=3;s="'+type_motion[0]+'_Motion"."'+type_motion[1]+'_WINCCOA_SeqNum"')).set_value(ua.DataValue(ua.Variant(self.motion_sequence[index], ua.VariantType.Byte)))
        self.client.get_node(ua.NodeId.from_string('ns=3;s="'+type_motion[0]+'_Motion"."'+type_motion[1]+'_EncToReach"')).set_value(ua.DataValue(ua.Variant(int(pos), ua.VariantType.Int32)))
        if "Ver" in type_motion[0]:
            precision=20
        else:
            precision=50
        self.client.get_node(ua.NodeId.from_string('ns=3;s="'+type_motion[0]+'_Motion"."'+type_motion[1]+'_CountPrecision"')).set_value(ua.DataValue(ua.Variant(int(precision), ua.VariantType.Int32)))
        self.client.get_node(ua.NodeId.from_string('ns=3;s="'+type_motion[0]+'_Motion"."'+type_motion[1]+'_RelAbsMove"')).set_value(ua.DataValue(ua.Variant(True, ua.VariantType.Boolean)))  
      #pos_string = str(int(pos*100))
      #message = self.visa_wr("MP" + pos_string + "\r")
    except TableContError as error:
      raise error
    
  def start_mov(self, index):
    try:
        type_motion=self.select_drive(index)
        self.client.get_node(ua.NodeId.from_string('ns=3;s="'+type_motion[0]+'_Motion"."WINCCOA_'+type_motion[1]+'_Stop"')).set_value(ua.DataValue(ua.Variant(True, ua.VariantType.Boolean)))
        self.client.get_node(ua.NodeId.from_string('ns=3;s="'+type_motion[0]+'_Motion"."WINCCOA_'+type_motion[1]+'_Start"')).set_value(ua.DataValue(ua.Variant(False, ua.VariantType.Boolean)))
        time.sleep(1)
        self.client.get_node(ua.NodeId.from_string('ns=3;s="'+type_motion[0]+'_Motion"."WINCCOA_'+type_motion[1]+'_Stop"')).set_value(ua.DataValue(ua.Variant(False, ua.VariantType.Boolean)))
        self.client.get_node(ua.NodeId.from_string('ns=3;s="'+type_motion[0]+'_Motion"."WINCCOA_'+type_motion[1]+'_Start"')).set_value(ua.DataValue(ua.Variant(True, ua.VariantType.Boolean)))
        time.sleep(1)
        print("start moving")
    except TableContError as error:
      raise error

  def stop_mov(self):
    try:
        print("Stopping the table")
        for index in 0,1:
            self.stop_drive_mov(index)
        time.sleep(1)
        stop_ind = self.check_stop()
        status =stop_ind[0] and stop_ind[1]
        while not status:
            stop_ind = self.check_stop()
            status =stop_ind[0] and stop_ind[1]
        time.sleep(1)
        for index in 0,1:
            self.actual_pos[index]=self.get_position(index)
    except TableContError as error:
      raise error

  def stop_drive_mov(self,index):
    try:
            type_motion=self.select_drive(index)
            self.client.get_node(ua.NodeId.from_string('ns=3;s="'+type_motion[0]+'_Motion"."WINCCOA_'+type_motion[1]+'_Stop"')).set_value(ua.DataValue(ua.Variant(True, ua.VariantType.Boolean)))
            self.client.get_node(ua.NodeId.from_string('ns=3;s="'+type_motion[0]+'_Motion"."WINCCOA_'+type_motion[1]+'_Start"')).set_value(ua.DataValue(ua.Variant(False, ua.VariantType.Boolean)))
            #print(self.client.get_node(ua.NodeId.from_string('ns=3;s="'+type_motion[0]+'_Motion"."WINCCOA_'+type_motion[1]+'_Stop"')).get_value())
            #print(self.client.get_node(ua.NodeId.from_string('ns=3;s="'+type_motion[0]+'_Motion"."WINCCOA_'+type_motion[1]+'_Start"')).get_value())
            print("Stop")
    except TableContError as error:
      raise error
  
  def home_mov(self):
    try:
        for index in 0,1:
            self.init_table(index)
            self.home_drive_mov(index)
        #self.check_stop()
        #for index in 0,1:
        #    if self.fast[index]:
        #        self.home_drive_mov(index) 
        #self.check_stop()
        #for index in 0,1:
        #    self.actual_pos[index]=self.get_position(index)
    except TableContError as error:
      raise error

  def home_drive_mov(self,index):
    try:
        #if(self.actual_pos[index]-3500<=0) or self.fast[index]:
            self.motion_sequence[index]=4
            self.set_precision_motion(index,0)
            self.start_mov(index)
        #elif (self.actual_pos[index]-3000>0):
            #self.motion_sequence[index]=2
            #self.fast[index]=True
            #self.set_position(3000,index)
            #self.start_mov(index)
    except TableContError as error:
      raise error

  def set_precision_motion(self,index,precision):
    try:
        type_motion=self.select_drive(index)
        self.client.get_node(ua.NodeId.from_string('ns=3;s="'+type_motion[0]+'_Motion"."'+type_motion[1]+'_CountPrecision"')).set_value(ua.DataValue(ua.Variant(int(precision), ua.VariantType.Int32))) 
        self.client.get_node(ua.NodeId.from_string('ns=3;s="'+type_motion[0]+'_Motion"."'+type_motion[1]+'_WINCCOA_SeqNum"')).set_value(ua.DataValue(ua.Variant(self.motion_sequence[index], ua.VariantType.Byte)))
        print(type_motion[0], self.motion_sequence[index])
    except TableContError as error:
      raise error
            
  def check_error_flag(self):
    try:
        for index in 0,1:
            self.error_flag[index]=False
            type_motion=self.select_drive(index)
            self.error_flag[index]= self.client.get_node(ua.NodeId.from_string('ns=3;s="'+type_motion[0]+'_Motion"."EncErr_'+type_motion[1]+'"')).get_value()
            #self.error_flag[index]= self.client.get_node(ua.NodeId.from_string('ns=3;s="'+type_motion[0]+'_Motion"."EncErrAck_'+type_motion[1]+'"')).get_value()
        return self.error_flag
    except TableContError as error:
      raise error
            
  def error_ack(self):
    try:
        for index in 0,1:
            type_motion=self.select_drive(index)
            self.client.get_node(ua.NodeId.from_string('ns=3;s="'+type_motion[0]+'_Motion"."EncErrAck_'+type_motion[1]+'"')).set_value(ua.DataValue(ua.Variant(False, ua.VariantType.Boolean)))
            time.sleep(0.5)
            self.client.get_node(ua.NodeId.from_string('ns=3;s="'+type_motion[0]+'_Motion"."EncErrAck_'+type_motion[1]+'"')).set_value(ua.DataValue(ua.Variant(True, ua.VariantType.Boolean)))
            time.sleep(0.5)
            self.client.get_node(ua.NodeId.from_string('ns=3;s="'+type_motion[0]+'_Motion"."EncErrAck_'+type_motion[1]+'"')).set_value(ua.DataValue(ua.Variant(False, ua.VariantType.Boolean)))    
    except TableContError as error:
      raise error

  def check_Yellow_box(self):
    try:
        self.yellow_box= self.client.get_node(ua.NodeId.from_string('ns=3;s="YellowBox_Override"')).get_value()
        return self.yellow_box
    except TableContError as error:
      raise error

### Template APP ### 
class TemplateApp:
  def __init__(self,argv):
    # For the moment we don't include a FSM, even if it is raccomended to 
    # develop one for python custom apps
    #self.final_state_machine = Fsm()
    
    self.socket_cycle_running = False

    self.rsdictReverse = { #imported from H4DAQ/interface/Command.hpp
      'START': 0,
      'INIT': 1,
      'INITIALIZED': 2,
      'BEGINSPILL': 3,
      'CLEARED': 4,
      'WAITFORREADY': 5,
      'CLEARBUSY': 6,
      'WAITTRIG': 7,
      'READ': 8,
      'ENDSPILL': 9,
      'RECVBUFFER': 10,
      'SENTBUFFER': 11,
      'SPILLCOMPLETED': 12,
      'BYE': 13,
      'ERROR': 14
    }

    self.status = 'INIT'

    self.data_port = 0
    self.status_port = 0
    self.cmd_port = 0

    self.sub = None # Receives commands
    self.pub = None # Send status 
    self.context = None
    self.poller = None
    self.sub_urls = []
    
    self.tableCont = TableController()
    self.move_table = False	#flag for if message contains command to move table, handeled in proc_message
    self.stop_table = False
    self.home_table = False 
    self.stop_status = False
    self.fisrt_mov = True 
    self.second_stop = False
    self.flag_error = False
    self.ack_table = False
    self.get_position_table = False
    self.local_mode = False
    self.wrong_position = False 

    # Parsing arguments coming from the AppControl
    # We expect -c and the configuration file path
    # and -l with the log file path
    # An error occurs if they are missing 
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--configurefile", type=str, help="Configuration file", required=True)
    parser.add_argument("-l", "--logfile", type=str, help="Log file", required=True)
    args = parser.parse_args()
    self.configure_file_path = args.configurefile
    self.log_file_path = args.logfile
    self.tableCont.log_file = self.log_file_path

    # Redirecting output to log file
    sys.stdout = open(self.log_file_path, 'w')
        


  def listen_socket(self):
    print("Entering in socket cycle")
    while self.socket_cycle_running:
      try:
        # Limiting the loop rate with 5 sec sleep
        time.sleep(5)
        # Sending state
        status_msg = "STATUS statuscode="+str(self.rsdictReverse[self.status])
        print("Sending status msg: ",status_msg)
        self.pub.send_string(status_msg)

        # Polling for messages
        socks = dict(self.poller.poll(1))
        if socks.get(self.sub):
          message = self.sub.recv()
          message = message.decode("utf-8")
          self.proc_message(message)
                
         
        # It seems that python is not flushing "print" each time it is called.
        # Forcing to do it once per cycle.

        sys.stdout.flush()
      except:
        print("Error in the loop. Breaking...")
        self.socket_cycle_running = False
        sys.stdout.flush()

      try: 
        flags=self.tableCont.check_error_flag()
        self.flag_error=flags[0] or flags[1]
        #print("Errore", self.flag_error, flags[0], flags[1])
        while (self.flag_error):
          #print("Errore", self.flag_error, flags[0], flags[1])
          self.tableCont.table_display = "FLAG_ERROR"
          self.pub.send_string(self.tableCont.table_display)
          time.sleep(1)
          self.call_sock()
          flags=self.tableCont.check_error_flag()
          self.flag_error=flags[0] or flags[1]
          sys.stdout.flush()
          if self.ack_table:
              break
      except:
        print("Flag error table not working. Breaking...", str)
        self.socket_cycle_running = False
        sys.stdout.flush()
              
      try: 
        self.local_mode = self.tableCont.check_Yellow_box()
        while self.local_mode:
            print("TABLE IN LOCAL MODE")
            self.tableCont.table_display = "LOCAL"
            self.pub.send_string(self.tableCont.table_display) #SEND TO GUI
            self.local_mode = self.tableCont.check_Yellow_box() 
            time.sleep(5)
            sys.stdout.flush()
      except:
        print("Check local mode not working. Breaking...", str)
        self.socket_cycle_running = False
        sys.stdout.flush()
      ###Table controller while loop###
      try: 
        if self.move_table and not self.flag_error: 	#flag changed in proc_message
          #if not self.tableCont.table_sim:           
          #  self.tableCont.init_table()
            #Assuming visa close done in init_table
          #set actual position taken care of in init_table or automatic if sim
          #send actual position zmq
          table_pos_data = "TAB_IS " + str(self.tableCont.actual_pos[0]) + " " + str(self.tableCont.actual_pos[1])
          self.pub.send_string(table_pos_data) #SEND TO THE GUI 
          index_pos = message.find("SET_TABLE_POSITION ")
          if index_pos >=0:
            self.tableCont.run = True
             #SEND STATUS TO GUI
            #self.tableCont.table_display = "TAB_MOVING"
            #self.pub.send_string(self.tableCont.table_display) #SEND TO GUI
            #set move position
            mes_pos = message.replace('SET_TABLE_POSITION ', '')
            self.tableCont.move_pos = [float(i) for i in (mes_pos.split(" "))]
            #check if table simulation
            if self.tableCont.table_sim:
                self.wrong_position = self.tableCont.move_to_pos()
                if self.wrong_position:
                    table_outside_bound = "TAB_POS_WRONG"
                    self.pub.send_string(table_outside_bound)
                    self.move_table = False
                    sys.stdout.flush()
                    self.tableCont.table_display = "TAB_DONE"
                    self.pub.send_string(self.tableCont.table_display)
                    continue 
                print("Table is moving")
                self.call_sock()
                while (not self.stop_table):
                    self.call_sock()
                    while (not self.stop_status):
                        self.tableCont.table_display = "TAB_MOVING"
                        self.pub.send_string(self.tableCont.table_display) #SEND TO GUI
                        stopps=self.tableCont.check_stop()
                        self.stop_status = stopps[0] and stopps[1]
                        flags=self.tableCont.check_error_flag()
                        self.flag_error=flags[0] or flags[1]
                        for index in 0,1:
                            self.tableCont.actual_pos[index]=self.tableCont.get_position(index)
                        table_pos_data = "TAB_IS " + str(self.tableCont.actual_pos[0]) + " " + str(self.tableCont.actual_pos[1])
                        self.pub.send_string(table_pos_data) #SEND TO THE GUI
                        time.sleep(1)
                        self.call_sock()
                        if self.stop_table or self.flag_error:
                            self.tableCont.stop_mov()
                            self.second_stop = True
                            time.sleep(1)
                            break
                    if self.stop_table:
                        break
                    self.call_sock()
                    for index in 0,1:
                        if self.tableCont.fast[index] and not self.second_stop:
                            self.first_mov = False
                            self.stop_status = False
                            print("slow movement")
                            self.wrong_position = self.tableCont.move_to_pos(self.first_mov)
                    self.call_sock()
                    print("perche' esci")
                    flags=self.tableCont.check_error_flag()
                    self.flag_error=flags[0] or flags[1]
                    while (not self.stop_status):
                        self.tableCont.table_display = "TAB_MOVING"
                        self.pub.send_string(self.tableCont.table_display) #SEND TO GUI
                        stopps=self.tableCont.check_stop()
                        self.stop_status = stopps[0] and stopps[1]
                        flags=self.tableCont.check_error_flag()
                        self.flag_error=flags[0] or flags[1]
                        for index in 0,1:
                            self.tableCont.actual_pos[index]=self.tableCont.get_position(index)
                        table_pos_data = "TAB_IS " + str(self.tableCont.actual_pos[0]) + " " + str(self.tableCont.actual_pos[1])
                        self.pub.send_string(table_pos_data) #SEND TO THE GUI
                        time.sleep(1)
                        self.call_sock()
                        if self.stop_table or self.flag_error:
                            self.tableCont.stop_mov()
                            time.sleep(1)
                            break
                    for index in 0,1:
                        self.tableCont.actual_pos[index]=self.tableCont.get_position(index)
                    self.stop_table=True
                #time.sleep(5)
              #self.tableCont.actual_pos = self.tableCont.move_pos[:]
            #else:	#if not, move the table
            #  time.sleep(2)
            #    print("moving table 2")
            #    self.tableCont.move_to_pos()
            #finished moving table
            self.stop_table=False
            self.first_move = True
            self.stop_status = False
            self.second_stop=False
            self.tableCont.run = False
            self.tableCont.table_display = "TAB_DONE"
            self.pub.send_string(self.tableCont.table_display)
            self.move_table = False
            print("Table Position: ",self.tableCont.actual_pos)
            print("Sending last position to the GUI")
            table_pos_data = "TAB_IS " + str(self.tableCont.actual_pos[0]) + " " + str(self.tableCont.actual_pos[1])
            self.pub.send_string(table_pos_data) #SEND TO THE GUI
        else:
          # We update table pos at each status update if the table is not moving
          table_pos_data = "TAB_IS " + str(self.tableCont.actual_pos[0]) + " " + str(self.tableCont.actual_pos[1])
          self.pub.send_string(table_pos_data) #SEND TO THE GUI
      except TableContError as error:
        print(str(error))
        self.socket_cycle_running = False
        sys.stdout.flush()
      except ValueError as error:
        print('ValueeRROR', str(error))
      except serial.SerialException as error:
        print('Serial exception', str(error))
      except:
        print("Error moving table. Breaking...", str)
        self.socket_cycle_running = False
        sys.stdout.flush()
      try: 
        if self.stop_table:	#flag changed in proc_message
            if self.tableCont.table_sim:
                self.tableCont.stop_mov()
            self.stop_table=False
            self.tableCont.table_display = "TAB_DONE"
            self.pub.send_string(self.tableCont.table_display)
            print("Table Position: ",self.tableCont.actual_pos)
            print("Sending last position to the GUI")
            table_pos_data = "TAB_IS " + str(self.tableCont.actual_pos[0]) + " " + str(self.tableCont.actual_pos[1])
            self.pub.send_string(table_pos_data) #SEND TO THE GUI
      except:
        print("Error stoping table. Breaking...", str)
        self.socket_cycle_running = False
        sys.stdout.flush()
      try: 
        if self.home_table:	#flag changed in proc_message
          table_pos_data = "TAB_IS " + str(self.tableCont.actual_pos[0]) + " " + str(self.tableCont.actual_pos[1])
          self.pub.send_string(table_pos_data) #SEND TO THE GUI 
          index_pos = message.find("HOME_TABLE")
          if index_pos >=0:
            self.tableCont.run = True
             #SEND STATUS TO GUI
            self.tableCont.table_display = "TAB_MOVING"
            self.pub.send_string(self.tableCont.table_display) #SEND TO GUI
            #set move position
            #mes_pos = message.replace('SET_TABLE_POSITION ', '')
            #self.tableCont.move_pos = [float(i) for i in (mes_pos.split(" "))]
            if self.tableCont.table_sim:
                flags=self.tableCont.check_error_flag()
                self.flag_error=flags[0] or flags[1]
                if self.flag_error:
                    break
                self.tableCont.home_mov()
                print("Table is homing")
                self.call_sock()
                while (not self.stop_table and not self.flag_error):
                    self.call_sock()
                    while (not self.stop_status):
                        self.tableCont.table_display = "TAB_MOVING"
                        self.pub.send_string(self.tableCont.table_display) #SEND TO GUI
                        for index in 0,1:
                            self.tableCont.actual_pos[index]=self.tableCont.get_position(index)
                        table_pos_data = "TAB_IS " + str(self.tableCont.actual_pos[0]) + " " + str(self.tableCont.actual_pos[1])
                        self.pub.send_string(table_pos_data) #SEND TO THE GUI
                        stopps=self.tableCont.check_stop()
                        self.stop_status = stopps[0] and stopps[1]
                        time.sleep(1)
                        self.call_sock()
                        flags=self.tableCont.check_error_flag()
                        self.flag_error=flags[0] or flags[1]
                        if self.stop_table or self.flag_error:
                            self.tableCont.stop_mov()
                            break
                    print("moving table")
                    self.call_sock()
                    for index in 0,1:
                        self.tableCont.actual_pos[index]=self.tableCont.get_position(index)
                    self.stop_table=True
            self.stop_table=False
            self.stop_status = False
            self.first_move = True
            self.second_stop=False
            self.tableCont.run = False
            if self.flag_error:
                break
            self.tableCont.table_display = "TAB_HOME"
            self.pub.send_string(self.tableCont.table_display)
            time.sleep(2)
            self.tableCont.table_display = "TAB_DONE"
            self.pub.send_string(self.tableCont.table_display)
            self.home_table = False
            print("Table Position: ",self.tableCont.actual_pos)
            print("Sending last position to the GUI")
            table_pos_data = "TAB_IS " + str(self.tableCont.actual_pos[0]) + " " + str(self.tableCont.actual_pos[1])
            self.pub.send_string(table_pos_data) #SEND TO THE GUI
      except:
        print("Error homing table. Breaking...", str)
        self.socket_cycle_running = False
        sys.stdout.flush()
      try: 
        if self.ack_table:	#flag changed in proc_message
            self.tableCont.error_ack()
            flags=self.tableCont.check_error_flag()
            self.flag_error=flags[0] or flags[1]
            if self.flag_error:
                    raise TableContError("Acknowledge not working contact experts.")
            self.tableCont.table_display = "ACK_ERROR_FLAG"
            self.pub.send_string(self.tableCont.table_display) 
      except:
        print("Error acknowledging table. Breaking...", str)
        self.socket_cycle_running = False
        sys.stdout.flush() 
      try: 
        if self.get_position_table:
            for index in 0,1:
                self.tableCont.actual_pos[index]=self.tableCont.get_position(index)
            self.tableCont.table_display = "TAB_DONE"
            self.pub.send_string(self.tableCont.table_display)
            self.get_position_table = False 
            print("Table Position: ",self.tableCont.actual_pos)
            print("Sending last position to the GUI")
            table_pos_data = "TAB_IS " + str(self.tableCont.actual_pos[0]) + " " + str(self.tableCont.actual_pos[1])
            self.pub.send_string(table_pos_data) #SEND TO THE GUI
      except:
        print("Error getting position of the table. Breaking...", str)
        self.socket_cycle_running = False
        sys.stdout.flush() 

  def call_sock(self):
    socks = dict(self.poller.poll(1))
    if socks.get(self.sub):
            print("sock is ok")
            message = self.sub.recv()
            message = message.decode("utf-8")
            self.proc_message(message)
          
  def proc_message(self, msg):
    print(msg) 
    if 'SET_TABLE_POSITION' in msg:
      print('Request to move the table.') 
      
      #change flag to move table
      self.move_table = True
      print('movetableflagsettotrue')
    if 'STOP_TABLE' in msg:
        print('Request to stop the table.')
        self.stop_table = True
        self.move_table = False
        print("stoptableflagtotrue")
    if 'HOME_TABLE' in msg:
        print('Request to home the table.')
        self.home_table = True
        print("hometableflagtotrue")
    if 'ACK_TABLE' in msg:
        print('Request to ackowledge the flag error of the table.')
        self.ack_table = True
        print("acktableflagtotrue")
    if 'GET_TABLE_POSITION' in msg:
        print('Request the table position the flag error of the table.')
        self.get_position_table = True
        print("getpositiontableflagtotrue")
    if 'KILL_APP' in msg:
      print('Request to kill the app')
      self.socket_cycle_running = False


  def parse_standard_params(self, soup):
    # We parse the config file to extracts port numbers.
    print("Looking for ports...")
    port_list = []
    for tag in soup.find_all("ListenPort"):
      port_list.append(tag.string)
    self.data_port = port_list[0]
    self.status_port = port_list[1]
    self.cmd_port = port_list[2]
    print("Data port: ",self.data_port," - Status port: ",self.status_port," - Cmd port: ",self.cmd_port)

    # Url list for ZMQ to subscribe to:
    for tag in soup.find_all("ConnectTo"):
      self.sub_urls.append(tag.string)
     
    for tag in soup.find_all("SerialName"):
      self.tableCont.serial_name = tag.string


  # In this function you can extract other parameters from the config file
  def parse_custom_params(self, soup):
    print("Parsing custom params...")
    # Write here


  def parse_configure_file(self):
    try:
      print("Configure file path sent by AppControl: ",self.configure_file_path)
      with open(self.configure_file_path) as fp:
        soup = Soup(fp, "xml")
        self.parse_standard_params(soup)
        #self.parse_custom_params(soup)
      
    except:
      raise Exception("parse_configure_file failed")


  def initialize_zmq_channels(self):
    self.context = Context()
    self.poller = Poller()
    self.sub = self.context.socket(SUB)
  
    # If the application run in local... do we need to rename the url
    # with localhost instead of machine name? Let's do it...

    # First: subscription to the sockets where the application receives commands
    gui_hostname = socket.gethostname()
    for url in self.sub_urls:
      new_url = "tcp://"
      if gui_hostname in url:
        url = url.replace(gui_hostname,"localhost")
      new_url += url
      self.sub.connect(new_url)
    self.sub.setsockopt(SUBSCRIBE,b'')
    self.poller.register(self.sub,POLLIN)

    # Second: generate socket to send status
    self.pub = self.context.socket(PUB)
    new_url = "tcp://*:"
 #   port_index = self.status_port.find(":")
#    new_url = new_url + self.status_port[port_index+1:]
    new_url = new_url + self.status_port
    print("Url to publish app status: ", new_url)
    self.pub.bind(new_url)


### Actions - from FSM list ###

  def initialize(self):
    print("initialize action begin")
    self.parse_configure_file()
    self.initialize_zmq_channels()
    self.socket_cycle_running = True
    
    #Do initialize table controller stuff
    self.tableCont.initialize_tab_cont()
   
    # After initialize_tab_cont, the table is initialized and the current position has been updated
    # So actual_pos should contain updated data
    table_pos_data = "TAB_IS " + str(self.tableCont.actual_pos[0]) + " " + str(self.tableCont.actual_pos[1])
    self.pub.send_string(table_pos_data) #SEND TO THE GUI 

    self.status='INITIALIZED'
    print("initialize action end")

  def configure(self):
    print("configure action begin")
    print("configure action end")

  def start(self):
    print("start action begin")
    print("start action end")

  def stop(self):
    print("stop action begin")
    print("stop action end")

  def pause(self):
    print("pause action begin")
    print("pause action end")

  def resume(self):
    print("resume action begin")
    print("resume action end")

  def fail(self):
    print("fail action begin")
    self.socket_cycle_running = False
    print("fail action end")

  def reset(self):
    print("reset action begin")
    print("reset action end")



### Main ###

def main(argv):
  #Transferring frame 0 from labview pseudocode
  print(argv)
  newApp = TemplateApp(argv)
  
  # At first, the idea was to have a separate thread listening to the 
  # sockets and sending info. But if the application is killed the thread
  # would have continued to run.
  # So it has been moved inside function listen_socket.
  newApp.initialize()
  newApp.listen_socket()
  


if __name__ == '__main__':
  main(sys.argv)

