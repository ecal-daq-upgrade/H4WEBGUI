// Legend
// ru = readout unit; ch = channel; sm = supermodule; bcp = BCP; idx = index
//
// Coordinate systems:
// - SM:
//   * ch -> 1..1700; ch_ieta -> 1..85; ch_iphi -> 1..20;
//   * ru -> 1..68;   ru_ieta -> 1..17; ru_iphi -> 1..5;
// - RU
//   * ch -> 1..25; ch_ieta -> 1..5; ch_iphi -> 1..5;
//
// All numberings below start at 1.
// Array indices start at 0 but the first element is not used
//
// For the TB 2022, the BCP_ch_idx goes from 0 to 224 following the
// conventions below for the RU ordering and RU type.
// E.g. channel 8 is in SM_RU 41, which is of type ((41 - 4) / 8) % 2 = 0,
// i.e. type right

// RU number from 1 to 68, in the ordering used by the BCP
// i.e. the BCP_RU_idx = BCP_channel_idx / 25
const nMaxRu = 69
var bcpRUtoSMRUmap = {}
initMaps()

export function initMaps () {
  // SM_tt_idx goes from 1 to 68
  bcpRUtoSMRUmap[1] = {}
  bcpRUtoSMRUmap[1][0] = 42
  bcpRUtoSMRUmap[1][1] = 39
  bcpRUtoSMRUmap[1][2] = undefined
  bcpRUtoSMRUmap[1][3] = 37
  bcpRUtoSMRUmap[1][4] = 38
  bcpRUtoSMRUmap[1][5] = 41
  bcpRUtoSMRUmap[2] = {}
  bcpRUtoSMRUmap[2][0] = 43
  bcpRUtoSMRUmap[2][1] = 45
  bcpRUtoSMRUmap[2][2] = 46
  bcpRUtoSMRUmap[2][3] = undefined
  bcpRUtoSMRUmap[2][4] = 47
  bcpRUtoSMRUmap[2][5] = 69
}

// SM_RU_type === BCP_RU_type and either
// left              or  right
// |  5  6 15 16 25      21 20 11 10  0  |
// L  4  7 14 17 24      22 19 12  7  1  L
// V  3  8 13 18 23      23 18 13  6  2  V
// R  2  9 12 19 22      24 17 14  5  3  R
// |  1 10 11 20 21      25 16 15  4  4  |
//
// depending on the LVR position when looking at the electronics from
// the back of a SM (i.e. the beam is coming towards us from the
// front). Numbering from SM starts from 1 (as above) for BCP from 0.

// SM RU numbering
//  4 8 .. 68
//  ...
//  1 5 .. 65

// get the type of RU (0=right, 1=left)
// from the RU index within the SM (1..68)
export function smRuType (smRuIdx) {
  --smRuIdx
  return (Math.floor(Math.abs(smRuIdx - 4) / 8)) % 2
  // return (Math.abs(smRuIdx - 4) / 8) % 2
}

// get the ieta of the RU in the SM (1..17)
// from the RU index within the SM (1..68)
export function smRuIeta (smRuIdx) {
  --smRuIdx
  return Math.floor(smRuIdx / 4) + 1
}

// get the iphi of the RU in the SM (1..4)
// from the RU index within the SM (1..68)
export function smRuIphi (smRuIdx) {
  --smRuIdx
  return smRuIdx % 4 + 1
}

// get the ieta of the channel in the SM (1..85)
// from the channel index within the SM (1..1700)
export function smChIeta (smChIdx) {
  --smChIdx
  return Math.floor(smChIdx / 20) + 1
}

// get the iphi of the channel in the SM (1..20)
// from the channel index within the SM (1..1700)
export function smChIphi (smChIdx) {
  --smChIdx
  return smChIdx % 20 + 1
}

// get the ieta of the channel in the RU (1..5)
// from the channel index within the RU (1..25) and the RU type
export function ruChIeta (ruChIdx, ruType) {
  // ch_ieta within a RU is always equal to
  // 1 2 3 4 5
  // 1 2 3 4 5
  // 1 2 3 4 5
  // 1 2 3 4 5
  // 1 2 3 4 5
  --ruChIdx
  if (ruType) return Math.floor(ruChIdx / 5) + 1
  return 5 - Math.floor(ruChIdx / 5)
}

// get the iphi of the channel in the RU (1..5)
// from the channel index within the RU (1..25) and the RU type
export function ruChIphi (ruChIdx, ruType) {
  // ch_iphi within a RU is always equal to
  // 5 5 5 5 5
  // 4 4 4 4 4
  // 3 3 3 3 3
  // 2 2 2 2 2
  // 1 1 1 1 1
  --ruChIdx
  var ruChCol = Math.floor(ruChIdx / 5)
  var res = 0
  if (ruChCol % 2 === 0) res = ruChIdx % 5 + 1
  else res = 5 - ruChIdx % 5
  if (ruType) return res
  return 6 - res
}

// Conversions

// get the RU number within the SM from the channel index within the SM
export function smChToSmRu (smChIdx) {
  var ieta = smChIeta(smChIdx)
  var iphi = smChIphi(smChIdx)
  var smRuIeta = Math.floor((ieta - 1) / 5) + 1
  var smRuIphi = Math.floor((iphi - 1) / 5) + 1
  return (smRuIeta - 1) * 4 + smRuIphi
}

// get the channel number within a RU from the channel index within the SM
export function smChToRuCh (smChIdx) {
  var irueta = (smChIeta(smChIdx) - 1) % 5 + 1
  var iruphi = (smChIphi(smChIdx) - 1) % 5 + 1
  var ruType = smRuType(smChToSmRu(smChIdx))
  var res = 0
  if ((irueta - 1) % 2 === 0) res = iruphi + (irueta - 1) * 5
  else res = 6 - iruphi + (irueta - 1) * 5
  // return 6 - iruphi + (irueta - 1) * 5;
  if (ruType) return res
  return 26 - res
}

// get the channel index within the SM from the channel (ieta, iphi) within the SM
export function smChIetaIphiToSmCh (smChIeta, smChIphi) {
  --smChIeta
  --smChIphi
  return smChIeta * 20 + smChIphi + 1
}

// get the RU index within the BCP (0..8) from the channel index within the BCP (0..224)
export function bcpChToBcpRu (bcpChIdx) {
  return Math.floor(bcpChIdx / 25)
}

// get the channel index within the BCP RU (0..25) from the channel index within the BCP (0..224)
export function bcpChToBcpRuCh (bcpChIdx) {
  return bcpChIdx % 25
}

// get the channel index within the SM (1..1700) from the channel index within the BCP (0..224)
export function bcpChToSmCh (bcpNum, bcpChIdx) {
  var bcpRu = Math.floor(bcpChIdx / 25)
  var smRu = bcpRUtoSMRUmap[bcpNum][bcpRu]
  if (smRu === undefined) return undefined
  var smRuCh = bcpChToBcpRuCh(bcpChIdx) + 1
  // console.log('bcpChToSmCh ', bcpRu, smRu, smRuCh)
  var smChIeta = (smRuIeta(smRu) - 1) * 5 + ruChIeta(smRuCh, smRuType(smRu))
  var smChIphi = (smRuIphi(smRu) - 1) * 5 + ruChIphi(smRuCh, smRuType(smRu))
  //  return smChIetaIphiToSmCh(smChIeta, smChIphi);
  var idx = smChIetaIphiToSmCh(smChIeta, smChIphi)
  // console.log(bcpNum, bcpChIdx, bcpRu, smRu, smRuCh, smRuType(smRu), smChIeta, smChIphi, idx)
  return idx
}

// get the channel index within the BCP (0..224) from the channel index within the SM (1..1700)
export function smChToBcpCh (bcpNum, smChIdx) {
  var ru = smChToSmRu(smChIdx)
  var found = 0
  var iru
  for (iru = 0; iru < nMaxRu; ++iru) {
          if (bcpRUtoSMRUmap[bcpNum][iru] === ru) {
                  found = 1
                  break
          }
  }
  if (found) return 25 * iru + smChToRuCh(smChIdx) - 1
  return 0
}
